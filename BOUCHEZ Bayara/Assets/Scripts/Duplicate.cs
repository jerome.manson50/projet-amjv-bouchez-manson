﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duplicate : MonoBehaviour
{
    [SerializeField] private int dep_gauche;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame


    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0)){
            GameObject dub = Instantiate(gameObject);
            dub.transform.Translate(-dep_gauche, 0.0f, 0.0f, Space.World);
            Renderer rend_dub = dub.GetComponent<Renderer>();
            rend_dub.material.color = Color.green;
        }
    }
}
