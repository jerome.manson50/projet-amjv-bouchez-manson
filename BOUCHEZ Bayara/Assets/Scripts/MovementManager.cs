﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    [SerializeField]
    private Vector3 vitesse;
    [SerializeField]
    private float v_left;
    [SerializeField]
    private float v_right;
    [SerializeField]
    [Range (0,1)]
    private int repere;
    [SerializeField] private float dist_max;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey (KeyCode.R))
        {
            gameObject.transform.Rotate(vitesse);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            if (repere == 0)
            {
                gameObject.transform.Translate(v_left, 0.0f, 0.0f, Space.World);
            }
            else
            {
                gameObject.transform.Translate(v_left, 0.0f, 0.0f, Space.Self);
            }
        }

        if (Input.GetKey (KeyCode.D))
        {
            if (repere == 0)
            {
                gameObject.transform.Translate(-v_right, 0.0f, 0.0f, Space.World);
            }
            else
            {
                gameObject.transform.Translate(-v_right, 0.0f, 0.0f, Space.Self);
            }
        }
    }

    private void FixedUpdate()
    {
        Event   currentEvent = Event.current;

        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;
        Vector3 currentMousePos = new Vector3();
        currentMousePos.x = x;
        currentMousePos.y = y;
        float dist = 0;
        Vector3 pos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        dist = Vector3.Distance(pos, currentMousePos);
        Debug.Log(dist);
        
        if (dist > dist_max)
        {
            gameObject.transform.Rotate(0.6f, 0.0f, 0.0f);
            Debug.Log(0.6);
        }
        else
        {
            gameObject.transform.Rotate(1.8f, 0.0f, 0.0f);
            Debug.Log(1.8);
        }
    }
}
