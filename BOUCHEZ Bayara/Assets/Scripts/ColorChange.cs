﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    private Renderer rend;
    private Color coul_base;
    
    [SerializeField] private Color couleur;
    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
        coul_base = rend.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKey (KeyCode.C))
        {
            rend.material.color = couleur;
        }*/
    }

    void OnMouseOver()
    {
        rend.material.color = Color.red;
    }

    void OnMouseExit()
    {
        rend.material.color = coul_base;
    }
}
